import React from 'react'
import {render, fireEvent, waitFor, screen} from '@testing-library/react'
import '@testing-library/jest-dom'
import FormProduct from '../Views/Components/FormProduct';


test('Should render component FormProduct ', async () => {
    
    render(<FormProduct handleClose={()=>true} />)
    expect(screen.getByText('Nuevo Producto'))
    expect(screen.getByText('Nombre'))
    expect(screen.getByText('Descripción'))
    expect(screen.getByText('Precio'))
    expect(screen.getByText('Código'))
  })

  
  test('Should render component FormProduct - messages of validation ', async () => {
    
    render(<FormProduct handleClose={()=>true} />)
    fireEvent.click(screen.getByRole("button"))
    expect(screen.getByText('Nuevo Producto'))
  })