import { SnackbarEventAction } from './snackbarEvent';
import { ProductAction } from './product';
import { MovementsAction } from './movements';
import { ConfigAction } from './config';

export * from './config';
export * from './product';
export * from './movements';

export * from './snackbarEvent';

export type Action = ConfigAction | ProductAction | SnackbarEventAction | MovementsAction;
