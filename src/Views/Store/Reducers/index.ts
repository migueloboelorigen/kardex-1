import { History } from "history";
import { combineReducers } from "redux";

import * as productReducer from "./product";
import * as movementsReducer from "./movements";
import * as configReducer from './config';
import * as snackbarReducer from './snackbarEvent';

import { SnackbarEvent } from "../Types";


export interface RootState {
	drawerOpen: boolean;
	productList: productReducer.IStateProducts;
	movements: movementsReducer.IStateMovements;
    snackbarEvents: SnackbarEvent[];	
}

export default (history: History) =>
	combineReducers({
		...movementsReducer,
		...configReducer,
		...productReducer,
		...snackbarReducer,
	});
	