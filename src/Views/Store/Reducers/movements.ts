import { Movements } from '@Core/Domain/Entities/Movements';
import { ConfigActions } from '../Types/config';
import {  MovementsAction, MovementsActions } from '../Types/index';
import createReducer from './createReducer';

export interface IStateMovements {
	ListMovements: Movements[]
	movements:Movements | null 
}
const initialState: IStateMovements = {
	ListMovements:[],
	movements:null
}
export const movements = createReducer<IStateMovements>(initialState, {
	[MovementsActions.ADD_MOVEMENTS](state: IStateMovements, action: MovementsAction) {
		//@ts-ignore
		return {...state, ListMovements: [...action.payload]}
	},
	[MovementsActions.GETS_MOVEMENTSS](state: IStateMovements, action: MovementsAction) {
		//@ts-ignore
		return {...state, ListMovements: [...action.payload]}
	},
	[ConfigActions.PURGE_STATE](state: IStateMovements, action: MovementsAction) {
		return {...state};
	},
});
