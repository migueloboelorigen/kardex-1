import React from 'react';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { deepOrange } from '@material-ui/core/colors';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import { useAuth } from '../AuthContext';
import { Redirect, useHistory } from 'react-router';

const useStyles = makeStyles((theme: Theme) =>
	createStyles({
		orange: {
			color: theme.palette.getContrastText(deepOrange[500]),
			backgroundColor: deepOrange[500],
		},
		containerMenu: {
			width: '100%',
			display: 'flex',
			alignItems: 'center',
			justifyContent: 'flex-end',
		},
	})
);

export default function MenuLogout() {
	const classes = useStyles();
	const history = useHistory();
	const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
	const { logout, currentUser } = useAuth();

	const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		setAnchorEl(event.currentTarget);
	};

	async function handleLogout() {
		if (!!logout) {
			await logout();
			history.push('/login');
			history.replace('/login');
		}
	}

	const handleClose = () => {
		setAnchorEl(null);
	};
    
    if(!currentUser){
        return <Redirect to="/login" />
    }

	return (
		<div className={classes.containerMenu}>
			<div>
				<IconButton aria-label="" onClick={handleClick}>
					<Avatar alt="Remy Sharp" className={classes.orange}>
						HS
					</Avatar>
				</IconButton>
			</div>
			<Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleClose}>
				<MenuItem onClick={handleLogout}>Logout</MenuItem>
			</Menu>
		</div>
	);
}
