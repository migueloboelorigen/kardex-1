// prettier-ignore
import { Product } from "@Core/Domain/Entities/Product";
import { Paper, Table, TableBody, TableCell, TableHead, TableRow } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import * as React from "react";
import { useSelector, useDispatch } from 'react-redux';
import { useActions } from "../Store/Actions";
import * as ProductActions from "../Store/Actions/Product";

import { RootState } from "../Store/Reducers/index";
import { CurrencyConversion } from '../../Core/Utils';

export function ProductTable() {
	const classes = useStyles();
 const dispatch = useDispatch() 
	const productList = useSelector((state: RootState) => state.productList.ListProducts);
	const productActions = useActions(ProductActions);

	React.useEffect(() => {
		dispatch(productActions.getProducts);
	}, [dispatch]) 

	return (
		<Paper className={classes.paper}>
			<Table className={classes.table}>
				<TableHead>
					<TableRow>
						<TableCell padding="default">Código</TableCell>
						<TableCell padding="default">Nombre</TableCell>
						<TableCell padding="default">Precio</TableCell>
						<TableCell padding="default">Stock</TableCell>
					</TableRow>
				</TableHead>
				<TableBody>
					{!!productList && !!productList.map && productList.map((n: Product) => {
						return (
							<TableRow
								key={n.id}
								hover
							>
								<TableCell padding="none">{n.code}</TableCell>
								<TableCell padding="none">{n.name}</TableCell>
								<TableCell padding="none">{CurrencyConversion(`${n.price}`,"$",2)}</TableCell>
								<TableCell padding="none">{n.stock}</TableCell>
							</TableRow>
						);
					})}
				</TableBody>
			</Table>
		</Paper>
	);
}

const useStyles = makeStyles({
	paper: {
		minWidth: 260,
		overflowY: "scroll",
		height: "400px",
		display: "block",
		width: "100%",
		maxHeight: "64vh",
		padding: "15px"
	},
	table: {
		width: "100%",
	},
});
