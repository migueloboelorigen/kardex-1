import { Button, CircularProgress, Grid, makeStyles, TextField, Typography } from '@material-ui/core';
import { Field, Form, Formik } from 'formik';
import React, { FC } from 'react';
import { number, object, string } from 'yup';

const useStyles = makeStyles(() => ({
	item: {
		margin: '7px 0',
		'& .MuiFormControl-root': {
			margin: 0,
			'& .MuiInputBase-root ': {
				backgroundColor: 'white',
				borderRadius: 50,
				'& .MuiOutlinedInput-input': { padding: '5px 15px' },
			},
			'& .MuiFormHelperText-root': { marginTop: 0 },
		},
	},
	accept: {
		backgroundColor: '#193104',
		color: 'white',
		borderRadius: 25,
		padding: '3px 15px',
		boxShadow: 'none',
		marginBottom: 15,
		'&:hover': { backgroundColor: 'white', color: '#193104' },
	},
	button: { borderRadius: 25, padding: '3px 15px', boxShadow: 'none' },
	page: { height: '100vh' },

	login: {
		backgroundColor: 'rgba(255, 255, 255, 0.9)',
		position: 'relative',
		width: 450,
		height: '100%',
		padding: 30,
	},
	conten: {
		height: 'calc(100% - 85px)',
		marginTop: 20,
		paddingTop: -1,
		paddingRight: 20,
		paddingBottom: 0,
		paddingLeft: 20,
	},
	footer: { '& b': { color: '#193104' } },
	imgLogo: {
		width: 197,
		height: 129,
		marginBottom: '-112.88px',
		position: 'relative',
		right: 102,
		top: '-42px',
	},
}));

 interface IInitialValues {
	name: string;
	description: string;
	price: number ;
	code: string;
}
const initialValues = {
	name: '',
	description: '',
	code: '',
	price: 0
};

interface IProps {
    handleClose:(values: IInitialValues) =>void
}

export const FormProduct: FC<IProps> = ({handleClose}) => {
	const classes = useStyles();
	const [loading, setLoading] = React.useState(false);
	const validations = object().shape({
		name: string().required('Campo requerido'),
		description: string().required('Campo requerido'),
		price: number().required('Campo requerido').positive("Numero Positivo"),
		code: string().required('Campo requerido'),
	});

	const onSubmit = async (values: IInitialValues) => {
        setLoading(true)
        try {
            await handleClose(values)
            setLoading(false)
        } catch (error) {
            setLoading(false)
        }
	};

	return (
		<Grid className={classes.conten} item container alignItems="center" xs={12}>
			<Grid container>
				<Formik 
                initialValues={initialValues} 
                validationSchema={validations} 
				validateOnMount={false}
                onSubmit={onSubmit}>
					{({ errors, touched }) => (
						<Form>
							<Typography variant="h6">
								<b>{'Nuevo Producto'}</b>
							</Typography>

							<Grid container>
								<Grid className={classes.item} item xs={12}>
								<Typography variant="caption" color="initial">Nombre</Typography>
									
									<Field
										as={TextField}
										name="name"
										variant="outlined"
										placeholder={'Nombre'}
										size="small"
										fullWidth
										autoFocus
										error={errors.name && touched.name}
										helperText={touched.name && errors.name}
									/>
								</Grid>
								<Grid className={classes.item} item xs={12}>
								<Typography variant="caption" color="initial">Descripción</Typography>

									<Field
										as={TextField}
										name="description"
										variant="outlined"
										placeholder={'Descripción'}
										size="small"
										fullWidth
										
										error={errors.description && touched.description}
										helperText={touched.description && errors.description}
									/>
								</Grid>
								<Grid className={classes.item} item xs={12}>
								
								<Typography variant="caption" color="initial">Precio</Typography>

									<Field
										as={TextField}
										type="number"
										name="price"
										variant="outlined"
										placeholder={'Precio'}
										size="small"
										fullWidth
										
										error={errors.price && touched.price}
										helperText={touched.price && errors.price}
									/>
								</Grid>
								<Grid className={classes.item} item xs={12}>
								<Typography variant="caption" color="initial">Código</Typography>
									<Field
										as={TextField}
										name="code"
										variant="outlined"
										placeholder={'Código'}
										size="small"
										fullWidth
										
										error={errors.code && touched.code}
										helperText={touched.code && errors.code}
									/>
								</Grid>
								<Grid className={classes.item} item xs={12}>
									<Button
										className={classes.accept}
										type="submit"
										variant="contained"
										disabled={loading}
										fullWidth
									>
										{!loading ? 'Registrar' : <CircularProgress size={25} />}
									</Button>
								</Grid>
							</Grid>
						</Form>
					)}
				</Formik>
			</Grid>
		</Grid>
	);
};

export default FormProduct;
