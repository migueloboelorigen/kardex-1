import React, { useContext, useState, useEffect } from "react"
import { RouterSwitch } from "react-typesafe-routes"
import { routerBlank } from "Router"
import { auth } from "../firebaseConfig"
import { UserRepositoryImpl } from '../Core/Infrastructure/Repositories/User/UserRepositoryImpl';
import { UserServiceImpl } from '../Core/Usecases/UserService';

type typeUser = {
    currentUser: firebase.default.User | null;
    value: firebase.default.User | null;
    login: (email:string, password:string)=> any | undefined;
    signup: any;
    logout: any;

}
const stateDefault = {
  currentUser: null,
  value: null,
  login: undefined,
  signup: null,
  logout: null,
}
const AuthContextProv = React.createContext<Partial<typeUser>>(stateDefault)

export function useAuth() {
  return useContext(AuthContextProv)
}

export function AuthProvider({ children }: any) {
  const userRepo = new UserRepositoryImpl();
  const userService = new UserServiceImpl(userRepo);
  const [currentUser, setCurrentUser] = useState<firebase.default.User | undefined >()
  const [loading, setLoading] = useState(true)

  function signup(email:string, password:string,nameUser: string) {
     return userService.signup(email, password,nameUser)
  }

  function login(email:string, password:string) {
    return userService.login(email, password)
  }

  function logout() {
    return userService.logout()
  }
  
  useEffect(() => {
     const unsubscribe = auth.onAuthStateChanged((user): any => {
       if(!!user){
         setCurrentUser(user)
         setLoading(false)
        }else{
          setCurrentUser(undefined)
         setLoading(true)
        }
    }) 

    return unsubscribe
  }, [])

  const value = {
    currentUser,
    login,
    signup,
    logout
  }

  return (
    <AuthContextProv.Provider value={value} >
				{!currentUser && <RouterSwitch router={routerBlank}  />}
					
      {!loading && children}
    </AuthContextProv.Provider>
    
  )
}