import { Entity } from '../Entity';

interface ProductProps {
  readonly id?: string
  readonly name: string
  readonly description: string
  readonly price: number
  readonly code: any
  readonly stock: number
}

export class Product extends Entity implements ProductProps {

  private readonly _name: string
  private readonly _description: string
  private readonly _price: number
  private readonly _code: any
  private readonly _stock: number

  constructor({id, name,  description, price, code,stock}: ProductProps) {
    super(id)
    this._name = name
    this._description = description
    this._price = price
    this._code = code
    this._stock = stock
  }

  get name(): string {
    return this._name;
  }

  get description(): string {
    return this._description;
  }

  get price(): number {
    return this._price
  }

  get code(): any {
    return this._code
  }

  get stock(): number {
    return this._stock
  }

  static create(props: ProductProps): Product {
    return new Product(props)
  }

}
