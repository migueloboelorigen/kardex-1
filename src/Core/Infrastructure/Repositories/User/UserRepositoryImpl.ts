import { UserRepository } from "./UserRepository";
import { FirebaseDataSource } from '../../DataSource/FirebaseDataSource';


export class UserRepositoryImpl implements UserRepository {

  async signup(email:string, password:string,nameUser: string): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.signup(email, password,nameUser)
  }

  async login(email:string, password:string): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.login(email, password)
  }
  
  async logout(): Promise<void> {
    const fireDataSource = new FirebaseDataSource()
    return  fireDataSource.logout()
  }
  

}
