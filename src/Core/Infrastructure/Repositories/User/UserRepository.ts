
export interface UserRepository {
  signup(email:string, password:string,nameUser: string): Promise<void | any>
  logout(): Promise<void | any>
  login(email:string, password:string): Promise<void | any>
}
