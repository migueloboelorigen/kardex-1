import { FirebaseDataSource } from "@Core/Infrastructure/DataSource/FirebaseDataSource";
import { calcStock } from "@Core/Utils";
import { MovementsRepository } from "@Repositories/Movements/MovementsRepository";
import { Movements } from '@Core/Domain/Entities/Movements';
export class MovementsRepositoryImpl implements MovementsRepository {
   
    async getMovements(): Promise<Movements[]>{
        const fireDataSource = new FirebaseDataSource()
         const response = await fireDataSource.get<any >("/movements")
         const repsonseArray = !!response ? Array.from(Object.values(response)): []; 
         const mapperMovements =  repsonseArray?.map((prodDb) => {
             // @ts-ignore
             const dataMovementMapper={  id: prodDb._id,  type: prodDb._type,  name: prodDb._name, cant: prodDb._cant, idProduct: prodDb._idProduct,}
             // @ts-ignore
            return new Movements(dataMovementMapper)}
             )
        return  mapperMovements
    }

    async addMovement(data: Movements): Promise<Movements | undefined> {
         const fireDataSource = new FirebaseDataSource()
         await fireDataSource.post("/movements",data)
         const respProduct = await fireDataSource.getByProperty<Movements | any>("/products/","_id",data.idProduct)
         // @ts-ignore
         const responseValues= Object.values(respProduct)[0]
         // @ts-ignore
         let newStock = calcStock(responseValues._stock, data);
         
         const responseKey= Object.keys(respProduct)[0]
        await fireDataSource.patch(`/products/${responseKey}`,{_stock:newStock})
        return  data
    }
}


