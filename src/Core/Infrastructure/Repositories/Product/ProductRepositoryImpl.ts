import { FirebaseDataSource } from "@Core/Infrastructure/DataSource/FirebaseDataSource";
import { ProductRepository } from "@Repositories/Product/ProductsRepository";
import { Product } from '../../../Domain/Entities/Product';

export class ProductRepositoryImpl implements ProductRepository {
   
    async getProducts(): Promise<Product[]>{
        const fireDataSource = new FirebaseDataSource()
         const response = await fireDataSource.get<any >("/products")
         const repsonseArray = !!response ? Array.from(Object.values(response)): []; 
         const mapperProducts =  repsonseArray?.map((prodDb) => {
             // @ts-ignore
             const dataProductMapper={ id: prodDb._id, name: prodDb._name, description: prodDb._description, price: prodDb._price, code: prodDb._code,stock: prodDb._stock}
             // @ts-ignore
            return new Product(dataProductMapper)}
             )
        return  mapperProducts
    }

    async addProduct(data: Product): Promise<Product | undefined> {
         const fireDataSource = new FirebaseDataSource()
         await fireDataSource.post("/products",data)
        return  data
    }
}