import { getDatabase, ref, get, update, remove,push,set,query,orderByChild,equalTo } from "firebase/database";
import { app,auth } from "firebaseConfig";
import {  updateProfile } from "firebase/auth";

const database = getDatabase(app);

export class FirebaseDataSource {
  private static instance: FirebaseDataSource
   constructor() {
  }

  static getInstance():FirebaseDataSource {
    if (!FirebaseDataSource.instance) FirebaseDataSource.instance = new FirebaseDataSource()
    return FirebaseDataSource.instance
  }

  
  async get<T>(uri: string): Promise<T | undefined> {
    const refDb = ref(database,`${uri}`);
      const resp = await get(refDb)
      return resp.val()
  }

  async getByProperty<T>(uri: string,  property: string, value: any): Promise<T | undefined> {
    const refDb = ref(database,`${uri}`);
    const queryRef = query(refDb, orderByChild(property),equalTo(value));
    const resp = await get(queryRef);
      return resp.val()
  }


  async post<T>(uri: string, data: any): Promise<void> {
    const postListRef = await ref(database, `${uri}`);
    const newPostRef = await push(postListRef);
    const resp = await  set(newPostRef, data);
    return resp
  }

  async patch<T>(uri: string, data: any): Promise<T | void > {
    const resp = await update(ref(database, `${uri}`), data);
    return resp
  }

  async put<T>(uri: string, data: any): Promise<T | void> {
    const resp = await update(ref(database, `${uri}`), data);
    return resp
  }

  async delete<T>(uri: string): Promise<T | void> {
    const resp = await remove(ref(database, `${uri}`));
    return resp
  }


  async signup(email:string, password:string,nameUser: string): Promise<void> {
    return auth.createUserWithEmailAndPassword(email, password).then(()=>{
      // @ts-ignore
      updateProfile(auth.currentUser, {  displayName: nameUser }
      )
    })
  }

  async login(email:string, password:string): Promise<any> {
    return auth.signInWithEmailAndPassword(email, password)
  }

  async logout(): Promise<void> {
    return auth.signOut()
  }

}
