import { UserRepository } from "@Repositories/User/UserRepository";

export interface UserService {
  signup(email:string, password:string,nameUser: string): Promise<void>
  logout(): Promise<void>
  login(email:string, password:string): Promise<void>
}

export class UserServiceImpl implements UserService {
  userRepo: UserRepository;

  constructor(ir: UserRepository) {
    this.userRepo = ir;
  }

  async signup(email:string, password:string,nameUser: string): Promise<void> {
    return this.userRepo.signup(email,password,nameUser);
  }

  async login(email:string, password:string): Promise<void> {
    return this.userRepo.login(email,password);
  }
  async logout(): Promise<void> {
    return this.userRepo.logout();
  }
}
