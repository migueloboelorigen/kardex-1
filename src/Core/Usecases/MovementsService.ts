import { Movements } from '@Entities/Movements';
import { MovementsRepository } from '@Repositories/Movements/MovementsRepository';

interface IMovementsService {
    getMovements(): Promise<Movements[]> 
    addMovements(data: Movements): Promise<Movements | undefined>
}

export class MovementsService implements IMovementsService {
    movementRepository: MovementsRepository
    constructor(pr: MovementsRepository) {
        this.movementRepository = pr
    }

    async getMovements (): Promise<Movements[]> {
        return this.movementRepository.getMovements()
    }

    async addMovements (data: Movements): Promise<Movements | undefined> {
        return this.movementRepository.addMovement(data)
    }
}