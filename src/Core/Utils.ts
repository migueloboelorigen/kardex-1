import { Movements } from './Domain/Entities/Movements';
export function calcStock(stock: number, data: Movements) {
    let newStock = 0;
    if (data.type === "Venta" && data.cant > stock) {
        throw new Error("Producto No Disponible");
    }
    if (data.type === "Venta") {
        newStock = stock - data.cant;
    }
    if (data.type === "Compra") {
        newStock = stock + data.cant;
    }
    return newStock;
}

export function CurrencyConversion(value_:string, currency:string, decimal:number) {
    var value = value_.toString().split('.')[0]
  
    value = value.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
  
    if (decimal > 0) {
      let partDecimal = parseFloat(value_).toFixed(decimal)
      partDecimal = partDecimal.split('.')[1]
      return currency + value + '.' + partDecimal
    }
  
    return currency + value
  }