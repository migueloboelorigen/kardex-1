describe('Form Product Test', () => {
	beforeEach(() => {
		cy.login();
		cy.visit('http://localhost:3000/products');
	});

	it('should be able to visit page products and have form Correct', () => {
		cy.get('#addProduct').click();
		cy.get('.MuiGrid-root:nth-child(1) > .MuiFormControl-root .MuiInputBase-input').type('nombre');
		cy.get('.MuiGrid-root:nth-child(2) > .MuiFormControl-root .MuiInputBase-input').type('descripcion');
		cy.get('.MuiGrid-root:nth-child(3) .MuiInputBase-input').type('100000');
		cy.get('.MuiGrid-root:nth-child(4) .MuiInputBase-input').type('ert122');
		cy.get('.makeStyles-accept-7 > .MuiButton-label').click();
		//cy.get('form').submit();
	});
	it('should be able to visit page products and have form Incorrect', () => {
		cy.get('#addProduct').click();
		
		cy.get('.MuiGrid-root:nth-child(2) > .MuiFormControl-root .MuiInputBase-input').type('descripcion');
		cy.get('.MuiGrid-root:nth-child(3) .MuiInputBase-input').type("-1");
		cy.get('.MuiGrid-root:nth-child(4) .MuiInputBase-input').type('ert122');
		cy.get('.makeStyles-accept-7 > .MuiButton-label').click();
        cy.contains('Campo requerido')
        cy.contains('Numero Positivo')
	});

});
