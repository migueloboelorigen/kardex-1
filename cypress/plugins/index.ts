/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

import codeCoverageTask from '@cypress/code-coverage/task';
const admin = require("firebase-admin")
const cypressFirebasePlugin = require("cypress-firebase").plugin;
// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */
export default (on: any, config: any) => {
	// `on` is used to hook into various events Cypress emits
	// `config` is the resolved Cypress config

	const extendedConfig = cypressFirebasePlugin(on,config,admin)
	codeCoverageTask(on, config);


	return extendedConfig;
};
